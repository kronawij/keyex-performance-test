#include <iostream>
#include <stdlib.h>
#include <string.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define ROUNDS 10

#define START 0
#define STOP  1

void die(const char* e) {
    std::cout << e << std::endl;
    exit(1);
}

int main(int argc, char *argv[])
{
    if (argc < 2) die("Please provide arguments");
    
    // create socket
    int socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socket_fd == -1) die("socket");

    // allow sending broadcasts
    int optval = 1;
    if (setsockopt(socket_fd, SOL_SOCKET, SO_BROADCAST, &optval, sizeof(int)) == -1) die("SO_BROADCAST");

    // bind socket to 0.0.0.0:8888
    struct sockaddr_in sa_me = { 0 };
    sa_me.sin_family = AF_INET;
    sa_me.sin_addr.s_addr = INADDR_ANY;
    sa_me.sin_port = htons(8888);
    if (bind(socket_fd, (struct sockaddr *)&sa_me, sizeof(struct sockaddr)) == -1) die("bind");

    char buffer[1024];

    // recipient specification (send to 255:255:255.255:7777)
    struct sockaddr_in sa_broadcast;
    sa_broadcast.sin_family = AF_INET;
    inet_aton("255.255.255.255",  &sa_broadcast.sin_addr);
    sa_broadcast.sin_port = htons(7777);
    socklen_t slen = sizeof(struct sockaddr);

    // responders
    struct sockaddr_in sa_other;

    std::string command(argv[1]);

    if (command.compare("START") == 0) {
        int nrEcus = 56;
        if (argc == 3) nrEcus = atoi(argv[2]);

        unsigned timeMS_max_avg = 0;

        for (int round = 0; round < ROUNDS; round++) {
            std::cout << "Round " << (round + 1) << ": Broadcasting start signal to " << nrEcus << " ECUs" << std::endl;
            
            // broadcast start signal to all ECUs
            buffer[0] = START;
            memcpy(buffer + 1, &nrEcus, sizeof(nrEcus));
            sendto(socket_fd, buffer, 5, 0, (struct sockaddr *) &sa_broadcast, slen);

            unsigned int timeMS_max = 0;
            bool allsuccess = true;

            for (int i = 0; i < nrEcus; i++) {
                unsigned int identifier;
                unsigned int ecuId;
                unsigned int err_code;
                unsigned int timeMS;

                while(1) {
                    if (recvfrom(socket_fd, buffer, 1024, 0, (struct sockaddr *) &sa_other, &slen) == -1) {
                        die("Recv error");
                    };

                    memcpy(&identifier, buffer + 0, 4);
                    memcpy(&ecuId, buffer + 4, 4);
                    memcpy(&err_code, buffer + 8, 4);
                    memcpy(&timeMS, buffer + 12, 4);

                    if (identifier == 0xF00D) {
                        break;
                    } else {
                        std::cout << "Ignoring packet" << std::endl;
                        continue;
                    }
                }

                if (err_code == 0) {
                    if (timeMS > timeMS_max) {
                        timeMS_max = timeMS;
                    }
                } else {
                    std::cout << "Key Ex failed for ECU " << ecuId << std::endl;
                    allsuccess = false;
                    break;
                }
            }

            if (allsuccess) {
                timeMS_max_avg += timeMS_max;
                std::cout << "Round maximum: " << timeMS_max << " ms" << std::endl;
            } else {
                break;
            }
        }

        timeMS_max_avg = timeMS_max_avg / ROUNDS; 
        std::cout << "All rounds complete. Average round maximum: " << timeMS_max_avg << " ms" << std::endl;
    }

    if (command.compare("STOP") == 0) {
        // broadcast stop signal to all ECUs
        std::cout << "Sending STOP to 255.255.255.255:7777" << std::endl;
        buffer[0] = STOP;
        sendto(socket_fd, buffer, 1, 0, (struct sockaddr *) &sa_broadcast, slen);
    }

    return 0;
}
