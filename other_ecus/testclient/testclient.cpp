#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <time.h>

//#define AUTOSTART

extern "C" {
#include <asoa_security_middleware/include/middleware.h>
}

#define START 0
#define STOP  1

void die(const char* e) {
    printf("%s\n", e);
    exit(1);
}

int main(int argc, char* argv[]) {
    int ecuId = -1;
    if (argc == 2) {
        ecuId = atoi(argv[1]);
    }

    asoasec_logging_init(INFO);

    int socket_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (socket_fd == -1) die("socket");

    struct sockaddr_in sa_me = { 0 };
    sa_me.sin_family = AF_INET;
    sa_me.sin_port = htons(7777);
    sa_me.sin_addr.s_addr = INADDR_ANY;

    // allow multiple process using port 7777
    int optval = 1;
    setsockopt(socket_fd, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof(int));

    if (bind(socket_fd, (struct sockaddr *)&sa_me, sizeof(struct sockaddr)) == -1) die("bind");

    struct sockaddr_in sa_other;
    socklen_t slen = sizeof(struct sockaddr);

    char buffer[1024] = { 0 };

#ifdef AUTOSTART
    sa_other.sin_family = AF_INET;
    sa_other.sin_port = htons(8888);
    inet_aton("132.231.14.104",  &sa_other.sin_addr);
#else
    while(1) {
        // wait for STARTTEST message
        recvfrom(socket_fd, buffer, 1024, 0, (struct sockaddr *)&sa_other, &slen);

        if (buffer[0] == STOP) {
            std::cout << "Received stop message" << std::endl;
            break;
        }

        if (buffer[0] != START) {
            std::cout << "Ignoring message" << std::endl;
            continue;
        }

        int nrEcus;
        memcpy(&nrEcus, buffer + 1, sizeof(nrEcus));
        
        std::cout << "Received START message (nrEcus = " << nrEcus << ")" << std::endl;
        if (ecuId >= nrEcus) continue;
#endif
        ASOA_MIDDLEWARE_RET_CODE err_code;

        ClientConfig config;
        err_code = get_client_config(&config);
        if (err_code != SUCCESS) {
            asoasec_log(ERROR, "Failed to read asoa_security.conf");
            return false;
        }

        struct timespec start, end;
        clock_gettime(CLOCK_REALTIME, &start);

        err_code = get_keys_from_server(&config);

        clock_gettime(CLOCK_REALTIME, &end);

        if (err_code == SUCCESS) {
            std::cout << "Keyex success" << std::endl;
        } else {
            std::cout << "Keyex failed" << std::endl;
        }


        unsigned int timeMS = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_nsec - start.tv_nsec) / 1000000.0;

        unsigned int identifier = 0xF00D;
        memcpy(buffer + 0, &identifier, 4);
        memcpy(buffer + 4, &ecuId, 4);
        memcpy(buffer + 8, &err_code, 4);
        memcpy(buffer + 12, &timeMS, 4);

        sa_other.sin_port = htons(8888);
        sendto(socket_fd, buffer, 16, 0, (sockaddr *) &sa_other, slen);
#ifndef AUTOSTART
    }
#endif

    asoasec_logging_destroy();

    return 0;
}
