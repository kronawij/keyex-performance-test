#!/bin/bash

ARM_TOOLCHAIN_ROOT=/opt/toolchains/cross-armhf
export ARMHF_ROOTFS=${ARM_TOOLCHAIN_ROOT}/rootfs
export RASPBERRY_VERSION=3

cmake -B build -S . -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE="${ARM_TOOLCHAIN_ROOT}/toolchain_armhf.cmake"
make -j`nproc` -C build
