#!/bin/bash

# copy AS config files to ecu2.local
# rsync -r "authserver_ecu/config" "pi@ecu2.local:/home/pi/jakob/loadtest"

ecu_ids=(4 5 6 8 10 12 14 15)

currentId=0
for ecu_id in ${ecu_ids[@]}; do
    address="ecu${ecu_id}.local"

    # install testclient
    # scp "other_ecus/testclient/build/testclient" "pi@${address}:/home/pi/jakob/loadtest" &

    remote_command=""

    for i in {0..6}
    do
        # install asoa_security.conf
        # scp "other_ecus/asoa_security_confs/asoa_security.conf.${currentId}" "pi@${address}:/home/pi/jakob/loadtest/ecu${currentId}/asoa_security.conf" &

        # prepare the remote command
        remote_command="${remote_command}(cd /home/pi/jakob/loadtest/ecu${currentId}; ../testclient ${currentId} < /dev/null > log 2>&1) & "

        currentId=$((currentId+1))
    done

    # run testclients
    # ssh "pi@${address}" "$remote_command" &
done

wait
