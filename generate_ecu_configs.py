import random

# This script generates the asoa_security.confs for the 56 ECUs and the corresponding ECU configs for the AS.
# The permission tree in file 'authserver_ecu/config/servicePermissions.xml' contains 512 permissions named perm0 to perm511

nrPermissions = 512
nrECUs = 56
allPerms = range(nrPermissions)

for x in range(nrECUs):
    ecu_config = '''<ECU>
    <ECU_ID>%d</ECU_ID>
    <ECU_Name>ECU_Monitor</ECU_Name>
    <Root_Key>01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01</Root_Key>
    
    <Services></Services>
    
    <MetaEndpoints>
''' % x

    asoa_security_conf = '''[general]
ecu_id = %d
ecu_name = "ECU_Monitor"

[network]
[network.auth_server]
transport = "UDP"
fallback_ip = "132.231.14.104"
fallback_port = 8080

[network.log_collector]
transport = "UDP"
fallback_ip = "132.231.14.101"
fallback_port = 8181

network_logging = false

[security]
root_key = "01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01"
fallback_key = "01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 10"

attempts_key_exchange = 3
salts=[
''' % x

    # add 32 salts to asoa_security.conf
    for _ in range(32):
        asoa_security_conf += '\t"01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 1F",\n'
    asoa_security_conf += "]\n"

    # take 20 permissions out of the 512 that exist
    samples = random.sample(allPerms, 20)
    for sample in samples:
        ecu_config += '\t\t<Endpoint name="perm%s" />\n' % sample

    ecu_config += "\t</MetaEndpoints>\n</ECU>\n"
  
    with open('authserver_ecu/config/ECUConfig/ecu%d.xml' % x, 'w') as file:
        file.write(ecu_config)

    with open('other_ecus/asoa_security_confs/asoa_security.conf.%d' % x, 'w') as file:
        file.write(asoa_security_conf)
