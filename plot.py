import matplotlib.pyplot as plt
import numpy as np
import csv

cols = []
rows = []

with open('results/results.csv', 'r') as file:
    csvreader = csv.reader(file)
    cols = next(csvreader)

    for row in csvreader:
        rows.append(row)

plt.rc('font', size = 8)
plt.rc("figure", dpi = 300)

fig, ax = plt.subplots()
fig.set_size_inches(12 / 2.54, 9 / 2.54)

ax.set(xlabel='N (Number of ECUs)', ylabel='Key exchange duration [ms]')
ax.grid()

x = np.array([1,8,16,24,32,40,48,56])
plt.xticks(x)

for i in range(len(cols)):
    colname = cols[i]

    if i == 0:
        colname = colname + " AS worker thread"
    else:
        colname = colname + " AS worker threads"

    # Data for plotting
    y = []
    for row in rows:
        y.append(int(row[i]))

    plt.plot(x, y, linestyle='dashed', marker='o', label=colname)

ax.legend()

plt.savefig("results/plot.png")
# plt.show()
